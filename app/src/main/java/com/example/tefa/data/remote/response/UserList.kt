package com.example.tefa.data.remote.response

import android.os.Parcelable
import com.example.tefa.data.model.UserItem
import kotlinx.parcelize.Parcelize

object UserList {
    @Parcelize
    data class Response(
        val page: Int?,
        val per_page: Int?,
        val total: Int?,
        val total_pages: Int?,
        val data: List<UserItem>
    ) : Parcelable
}