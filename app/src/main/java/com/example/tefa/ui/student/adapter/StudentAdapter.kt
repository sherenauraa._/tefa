package com.example.tefa.ui.student.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.tefa.data.model.UserItem
import com.example.tefa.databinding.ItemListUserBinding
import com.example.tefa.utils.load

class StudentAdapter(
    private var listItem: List<UserItem>,
    val actionClicked: ((data: UserItem) -> Unit?)? = null
) : RecyclerView.Adapter<StudentAdapter.CustomViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val binding =
            ItemListUserBinding.inflate(LayoutInflater.from(parent.context), parent, false)

        return CustomViewHolder(binding)
    }

    fun setData(newData: List<UserItem>) {
        listItem = newData
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        holder.bind(listItem[position])
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    inner class CustomViewHolder(val binding: ItemListUserBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(user: UserItem) {
            with(binding) {
                user.avatar?.let { ivAvatar.load(it) }
                tvName.text = "${user.first_name} ${user.last_name}"
                tvRole.text = "Student"
            }
        }
    }
}