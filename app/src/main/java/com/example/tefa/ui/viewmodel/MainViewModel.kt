package com.example.tefa.ui.viewmodel

import android.content.Context
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.tefa.data.model.UserItem
import com.example.tefa.data.remote.Repository
import com.example.tefa.data.remote.response.Login
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    @ApplicationContext private val context: Context,
    private val repository: Repository
) : ViewModel() {

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private val _userList = MutableLiveData<List<UserItem>>()
    val userList: LiveData<List<UserItem>> = _userList

    private val _isLogin = MutableLiveData<Login.Response>()
    val isLogin: LiveData<Login.Response> = _isLogin

    fun getUserList(page: Int) {
        viewModelScope.launch {
            try {
                _isLoading.postValue(true)
                val response = repository.getUserList(page)
                _userList.postValue(response)
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                _isLoading.postValue(false)
            }
        }
    }

    fun login(bodyRequest: Login.Request) {
        viewModelScope.launch {
            try {
                _isLoading.postValue(true)
                val response = repository.login(bodyRequest)
                _isLogin.postValue(response)
            } catch (e: Exception) {
                e.printStackTrace()
                Toast.makeText(context, "Login gagal", Toast.LENGTH_LONG).show()
            } finally {
                _isLoading.postValue(false)
            }
        }
    }

}