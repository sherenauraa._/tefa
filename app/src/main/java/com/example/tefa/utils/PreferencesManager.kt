package com.example.tefa.utils

import android.content.Context
import android.content.SharedPreferences
import com.example.tefa.utils.PreferencesKey.IS_FIRST_OPEN
import com.example.tefa.utils.PreferencesKey.IS_LOGGED_IN
import com.example.tefa.utils.PreferencesKey.USER_TOKEN

class PreferencesManager constructor(context: Context) {
    private val contextMode = Context.MODE_PRIVATE
    private var pref: SharedPreferences
    private var editor: SharedPreferences.Editor

    init {
        pref = context.getSharedPreferences("TEFA_PREFS", contextMode)
        editor = pref.edit()
        editor.apply()
    }

    var isFirstOpen: Boolean?
        get() {
            return pref.getBoolean(IS_FIRST_OPEN, true)
        }
        set(value) {
            if (value != null) {
                editor.putBoolean(IS_FIRST_OPEN, value)?.apply()
            }
        }

    var isLoggedIn: Boolean?
        get() {
            return pref.getBoolean(IS_LOGGED_IN, false)
        }
        set(value) {
            if (value != null) {
                editor.putBoolean(IS_LOGGED_IN, value)?.apply()
            }
        }

    var userToken: String?
        get() {
            return pref.getString(USER_TOKEN, "")
        }
        set(value) {
            if (value != null) {
                editor.putString(USER_TOKEN, value)?.apply()
            }
        }
}